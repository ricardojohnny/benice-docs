.. benice documentation master file, created by
   sphinx-quickstart on Wed Sep 18 15:17:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Benice Documentation
============================

.. meta::
   :description lang=en: Objetivo Data Lake (Pipeline de Dados).

Data Lake (Pipeline de Dados)
============================
**EEmovel - Inteligencia Imobiliaria**

**Project name: BENICE**

Objetivo
-----------------
A proposta inicial para a construção de um novo pipeline de dados foi trazer o máximo possível de dinamicidade ao processo, ser auto escalável e aliado ao melhor custo beneficio possível, onde a maioria dos processos operam via serverless.

Com o Data Lake formado, a EEmovel terá um padrão de ingresso e tratamento de dados sendo alimentando em varias frentes, gerando mais facilidade para o processo de implementação de projetos.

A stack e composta em 6 níveis de distribuição, sendo a captação, extração, normalização, geo localização, transformação dos dados e a entrega a nível de serviço.

O projeto **BENICE** (Data Lake da EEmovel) tem como plataforma base computacional e tecnológica, as soluções da **AWS - Amazon Web Services** .

Todas as informações aqui contidas foram extraídas com base no projeto e de seus respectivos desenvolvedores e mantenedores.


1. Da Captação
============================

1.1 Tecnologias Usadas:
-----------------------
- AWS Elastic Container Service (ECS)
- Docker
- AWS Kinesis Firehose
- AWS SQS

1.2 Objetivo
-------------
Parte do processo que chamamos de combustível para o pipeline, onde os dados são captados, convertidos e disponibilizados para a próxima parte do processo.

1.3 Spiders
-----------
O inicio do processo começa com um container docker[1] que a cada 30 minutos roda uma pesquisa no google capturando as imobiliárias que ainda não estao alocadas na base de imobiliarias e manda direto para uma coleção no '**data-acquisition.para-mapear**', em seguida começa o processo manual realizado pelo setor de DTA (Data Aquisition)  no mapeamento das imobiliarias e na criação das templates.

Após a imobiliaria ser mapeada, ela é comitada no repositorio 'benice-anuncios-spiders'[2] no momento que é dado push é enviado um GET para o webhook que irá sincronizar o Bitbucket com um  Bucket S3[3], após uma quantidade razoavel de commits  ou importancia, é rodado um 'pipeline'  para validar as spiders, remover duplicados, criar o registro dela no mongo '**benice-spiders.parameters_spiders**' e inserido um zip em outro bucket S3[4].

No próximo build do scrapy que acontece toda Terça-Feira(no ambiente de DEV) e toda Quinta-Feira(em PRD após o 'Ok' do running em DEV), é atualizada as spiders e a fila do SQS do Scrapy é realimentada.

Repos fonte
-----------
.. [1] = https://bitbucket.org/eemovel/data-acquisition-process/src/master/docker-novas-imobiliarias/
.. [2] = https://bitbucket.org/eemovel/benice-anuncios-spiders/src
.. [3] = https://s3.console.aws.amazon.com/s3/buckets/eemovel-bitbucket2s3-integration/benice-anuncios-spiders/?region=us-east-1
.. [4] = https://s3.console.aws.amazon.com/s3/buckets/benice-anuncios-spiders/?region=us-east-1&tab=overview


1.4 Scrapy Processo
-------------------
O processo Scrapy sao “tasks” em microservicos Docker(vide figura 1) hospedadas em um cluster **AWS ECS** que recebem os dados vindos das **Spiders(1.3)** atraves de uma fila do **AWS SQS**, e os envia para um coletor ou streaming de dados(**AWS Kinesis Firehose)** que os converte(.parquet format) e os disponibiliza em **AWS S3**  **Bucket**.

.. figure:: /img/captacao.png
    :align: center
    :figwidth: 600px
    :target: /img/captacao.png


Figura 1 - Imagem ilustrativa do processo de captacao


2. Processo de Nomalizacao
============================
2.1 Tecnologias Usadas:
-----------------------
- AWS Kinesis Firehose
- AWS Lambda
- AWS S3
- AWS GLUE

2.2 Objetivo
------------
O objetivo é colocar os dados de diferentes fontes em único padrão pronto para consumo.
O serviço se encontra em uma função do servico **AWS Lambda**, que é acionada toda vez que um batch de dados do processo de captura é disponibilizado pelo **AWS** **Kinesis** **Firehose**.

2.3 Pre-Normalizacao
--------------------
O processo se inicia em um sub-processo de  pré-normalização, as funções que estão relacionadas às normalizações “menos custosas” de dados se encontram no módulo **_normalize_data_**
A função **_pre_process_**  é responsável por descomprimir os dados, processar a categoria do imóvel anunciado e os valores de áreas e preços(venda, locação e/ou temporada).
Mais a  fundo o módulo **_features_**  contém os processamentos mais custosos, fazendo o trabalho “_sujo_” de procurar a categoria por similaridade de string, transformar as strings de valores que vêm em padrões diferentes em um único padrão.

.. note:: Arquivos descritos nesta sessão

          * **path:** normalizador/src/core/normalize_data_.py
          * **path:** normalizador/src/core/features.py



2.4 Processamento Categoria
---------------------------
Para procurar uma categoria, a função category utiliza dicionários python* que contém todas as categorias com seus respectivos id’s e também dicionários que separam essas em categorias superiores/subcategorias e as relacionam com seus respectivos aliases que nada mais são que possíveis nomes e/ou modos de escrita(também com possíveis erros de digitação) de cada uma das categorias de negócio que a EEmovel trabalha.

Existe também um additional_dicitonary com os alias para detectar imóveis em condomínio ou geminados, que podem aparecer tanto para casas quanto para sobrados.

A fim de performar o código, a busca começa sempre em dicionários menores, pois lógicamente é mais rápido percorrer uma lista com menos dados do que uma com mais. Logo a baixo segue a ordem de busca:

* Busca por imóvel comercial (variável commercial_category_dictionary);
* Busca por imóvel nem comercial, nem residencial (variável non_commercial_or_residential_dictionary);
* Busca por “extras” [variável additional_dictionary (em condomínio/geminado)];
* Busca por imóvel residencial (variável residential_category_dictionary);
* Busca por subcategoria de residencial (variável residential_category_dictionary);
* Busca por subcategoria de comercial (variável residential_category_dictionary);

.. note:: file path: normalizador/data/dictionaries/category.py


A função category sempre tem dois retornos, sendo categoria superior e subcategoria, se não houver subcategoria a categoria superior é replicada, isso ocorre pela forma como a plataforma SAAS mostrava/mostra a categoria do imóvel (sempre a sub).
Caso não seja possível o processamento de nenhuma categoria, é gerada uma exceção e o processo de normalização do registro não continua, e o mesmo é descartado.


2.5 Processamento dos valores de área e de transações
-----------------------------------------------------

2.5.1 Area
----------
Assim que a categoria é processada, é iniciado o processamento dos valores de área, e como existem vários campos para a área advindos dos dados brutos, foi criado uma equivalência entre os mesmos, como o exemplo a seguir: 

1. area_privativa pode ser area(área útil do imóvel para o nosso modelo) para apartamentos?
2. Area_terreno pode ser area_total(área total do imóvel para o nosso modelo) para casas?

Sendo possível a obtenção de pelo menos um valor de área (total_area ou area [área total e área útil respectivamente]) o processamento pode prosseguir, caso contrário uma exceção é gerada e o processamento do registro é descontinuado, e ele é descartado.

.. note:: file path: data/dictionaries/area_by_category.py

2.5.2 Valores de Transação
--------------------------
Existem campos para os três tipos de transação advindos dos dados brutos(valor_venda, valor_locacao e valor_temporada), e também um campo “geral”(valor), e quando só há esse campo, ele é considerado como valor de venda.
Após o processamento dos valores é feita uma análise de limites, para conferir se os valores de área, venda, locação, m² de venda e m² de locação estão nos intervalos aceitáveis.
Se houver valores para uma das transações e o mesmo estiver dentro dos limites o processo pode prosseguir, caso não tenha, é gerada uma exceção e caso esteja fora dos limites é gerado outra, com a normalização do item sendo interrompida e o registro descartado.
Após essas informações serem processadas com sucesso, o hash md5 da url do anúncio, o link_key, é processado e a pré-normalização finalizada, retornando ambos, tanto o dado bruto quanto o dado pré-processado.


.. note:: file path: data/limits.py

2.6 Normalização
----------------
Após a validacao dos campos necessários para que o registro seja adicionado a base de dados, é pego todos os link_key’s dos itens que obtiveram sucesso na pré-normalização e feito uma request em batch para o DynamoDB e retornar os itens que já constam em nossa base. Os mesmos são postos em um dicionário Python* com a chave de cada item sendo o link_key, assim a busca é mais rápida.

A função parse_one recebe ambos os dados pré-processados e os dados brutos retornados da funcao pre_process e então processa uma série de outras informações relacionados ao anúncio, como códigos, endereço, contatos, número de quartos, se é um imóvel privado dentre outras. Também é removido o aninhamento de campos, retirado campos nulos e força-se a tipagem de campos inteiros.

Após o processamento desses valores, é verificado se o item já consta na base, caso não tenha sido, o link_key do mesmo é adicionado a duas listas, uma de batch de mensagens a ser enviadas a fila SQS do serviço do amenities e outra para a fila SQS do serviço do geo.

Caso seja um registro que já consta na base, é feito um diff  para verificar a mudança de valores em quaisquer campo. Caso alguma informação de endereço/localização é alterada, marca-se como se o registro não tivesse informação de localização processadas pelo serviço do geo e envia-se ao SQS do mesmo, sendo o mesmo para o serviço do amenities caso a descrição do anúncio tenha sido alterada.

Se o anúncio é novo e for inserido, é retornado o status “Inserted” pela função, caso alguma informação do anúncio tenha sido alterada, é retornado o status  “Changed”, caso nada tenha sido alterado, é retornado o status “Updated”.

Em todos os casos o campo “updated_at” é setado para o horário que o registro foi processado, e o “expires_in” para exatos 10 dias após o horário em que o registro foi processado, para que assim o TTL do DynamoDB possa excluí-lo quando necessário.
O campo “created_at” somente é setado na primeira vez que o registro é processado.

.. note:: get_batch_by_link_keys em src/services/dynamo.py

          ** pipeline de funções na função parse_one em src/core/records.py



3. Processo Geo Localizacao
===========================

3.1 Tecnologias Usadas:
-----------------------
- AWS EC2
- AWS SQS 

3.2 Objetivo
------------
Esse processo esta atribuido junto ao processo de Normalizacao, que com base na informacao de endereco contida nos dados, ele as completa com as informacoes de latitude e longitude utilizando o servico interno Geo API e terceiros como o Google e Here.

3.3 Processo 
------------
O processo começa quando um batch de dados é recebido de uma fila do AWS SQS*, as mensagens sao carregadas e então é consultado os seus respectivos link_key's contidos nas mensagens e equiparadas com as do DynamoDB, so então é carregada as informações do anúncio, após esse processo as informações são passadas para uma função chamada “process_geographic_info" que conta quantos campos do documento passado não são nulos, se essa contagem for menor que 2 esse anúncio não será processado, será retornado Nulo, e sem sucesso, se a contagem for maior que 2 e o documento conter o original_address_raw, os campo serão verificados, o campo do endereço será separado do número, os campos “street”, “number”, “neighborhood”, “postal_code”,”city”, “state”, “lat”, “lon”, serão verificados e passados para um dicionário, mas se não houver o campo “city” ou “state”  será retornado Nulo, sem sucesso e logado 'REALTY_HAS_NO_CITY' ou 'REALTY_HAS_NO_STATE'.

3.4 Definicoes de Status
------------------------

3.4.1 Status 1
--------------
O dicionário, depois de preenchido com as informações de endereço do anúncio, vai para a lib do elasticsearch e lá se é definido o seu “status _endereco” pelos seguintes fatores: se conter  “city”,  “state”, “street” e “number” será definido como status 1(Full Address); 

3.4.2 Status 2
--------------
Se conter “lat” e “lon” e “street” ou “neighborhood” ele será definido como status 2(Reverse); 

3.4.3 Status 3
--------------
Se conter “city”,  “state”, “street” e não conter “number” ele será definido como status 3(Without Number); 

3.4.4 Status 4
--------------
Se conter “city”, “state”, “neighborhood” e não conter “street” será definido como status 4(Only Neighborhood); 

3.4.5 Status 5
--------------
Se não for possivel definir o status do endereço será definido como status 5(Indefinido); 

3.4.6 Status 6
--------------
.. important:: Se conter apenas os campos “postal_code”, “city” e “state” será definido como status 6 (Only Postal Code).

Se o status definido for diferente de 5 ou nada, será executada a query do respectivo status no ElasticSearch, e o resultado da pesquisa usando a query será comparado com o que foi pesquisado para a validação do resultado por meio da distância Levenshtein aplicada no python.

 Os campos validados são: 
* “street” -> “street_long”, 
* “neighborhood” -> “neighborhood” 
* “postal_code” -> “postal_code” 

Esses campos acima descritos são validados no caso de status 6(que também ocorre a validação do campo “country” para identificar se o endereço é brasileiro). 

Se validado estes campos a latitude e longitude do resultado é atribuída ao anúncio e ele é geo localizado.

.. note:: São “pinados” apenas os status menores que 4.

.. important:: Que para iniciar, requisita no sqs as credenciais necessárias para a pesquisa, e então de acordo com o “status _endereco” antes definido no processo do ElasticSearch é feita a pesquisa no HERE usando uma string de pesquisa: credentials, address["street"] + ", " + address["number"] + ", " + address["city"] + " - " + address["state"] +", BR"(exemplo Full Address).
               Quando obtido um resultado ele é convertido e comparado da mesma maneira que no processo do ElasticSearch, o procedimento de definição de latitude e longitude também é o mesmo,  se validado os campos, a latitude e longitude do resultado é atribuída ao anúncio e ele é geo localizado 

.. note:: São “pinados” apenas os status menores que 4.