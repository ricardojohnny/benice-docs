DA CAPTACAO
============================

.. meta::
   :description lang=en: Nivel Captacao.

Tecnologias Usadas:
-------------------------
* AWS Elastic Container Service (ECS)
* Docker
* AWS Kinesis Firehose
* AWS SQS

Esse nivel chamamos de combustivel para o processo, onde temos um cluster composto de varias maquinas e com varias tasks em 
microservicos que capta os dados e manda para uma fila de streaming de dados (KINESIS FIREHOSE).    x