## Environment branches

![GitFlow](img/data_science_gitflow.png)

.. figure:: img/data_science_gitflow.png
    :figwidth: 300px
    :target: img/data_science_gitflow.png

 São branches que correspondem por um ambiente, atualmente temos dois: ***master*** e ***dev***.

## Pull Request - PR

Ação de solicitar o merge de determinada em outra *branch*, nomenclatura utilizada no **Bitbucket** ( o nome pode variar dependendo do git server, ex gitlab = merge request ).

## Release branch

  - **Preffix** - toda branch de release deve ter o seguinte prefixo: release/, exemplo: release/sprint-20, release/2018-05-22

 - **Suffix** - o sufixo da branch deverá conter uma identificação da sprint e irá valer somente para aquela sprint
 - **Source branch** - a branch deverá ser criada, ESTRITAMENTE, a partir da branch master.
 - **Integration** - No termino da sprint um pull request será feito para master.
 - **Branch dependency** - em hipótese alguma deve ser feito merge de uma **environment branch** que não seja a **master**.	
 - **Maintainer** - 1 do time de dev e 1 do time de DevOps
 - **Branch updating** - o merge acontece de forma standalone, outras branches atualizam essa branch através de **PR**.

## Feature Branch
 - **Preffix** - toda branch de feature deve ter o seguinte prefixo: feature/, exemplo: feature/add-conditional-to-onboard
 
 - **Suffix** - o sufixo da branch deve conter ( em inglês ) a mudança que irá refletir no ambiente mergeado, exemplo feature/add-redirect-to-nginx.
 - **Source branch** - a branch deverá ser criada, ESTRITAMENTE, a partir da branch master.
 - **Integration** - Ao criar a branch o desenvolvedor irá abrir um **PR** para **dev** e **release branch vigente**. Poderá aceitar o PR em **dev** sempre que achar necessário para testar a publicação do seu código. O desenvolvedor ira testar sua **feature/fix**. Caso o teste seja bem sucedido e essa feature/fix venha a ser aceita, o **PR** para release branch é aceito, caso contrário o desenvolvedor irá retornar ao desenvolvimento após realizar novos PR's para **dev** e o fluxo será reiniciado.
 - **Maintainer** - desenvolvedor
 - **Branch dependency** - em hipótese alguma deve ser feito merge de uma environment branch que não seja a master. A dependência de outras branches deverá ser resolvida através de merges individuais. Exemplo: *feature/add-conditional-to-onboard* depende do redirect contido em *feature/add-redirect-to-nginx*, o desenvolvedor então irá executar os seguintes comandos:    
```bash
$ git checkout feature/add-conditional-to-onboard
$ git pull origin feature/add-conditional-to-onboard
$ git pull origin feature/add-redirect-to-nginx
```

- **Branch updating** - a atualização será feita através de merges periódicos na source branch ( master ) e na release branch ATUAL. 	JAMAIS deverá atualizar a branch com a outra branch a não ser essas.
## Fix branch
 - **Preffix** - toda branch de fix deve ter o seguinte prefixo: fix/, exemplo: fix/add-conditional-to-onboard
 
 - **Suffix** - o sufixo da branch deve conter ( em inglês ) a mudança que irá refletir no ambiente mergeado, exemplo fix/add-redirect-to-nginx.
 - **Source branch** - a branch deverá ser criada, ESTRITAMENTE, a partir da branch master.
 - **Integration** - Ao criar a branch o desenvolvedor irá abrir um **PR** para **dev** e **release branch vigente**. Poderá aceitar o PR em **dev** sempre que achar necessário para testar a publicação do seu código. O proprio desenvolvedor ira solicitar e testar sua **fix**. Caso o teste seja bem sucedido e essa **fix** venha a ser aceita, o **PR** para release branch é aceito, caso contrário o desenvolvedor irá retornar ao desenvolvimento após realizar novos PR's para **dev** e o fluxo será reiniciado.
 - **Maintainer** - desenvolvedor
 - **Branch dependency** - em hipótese alguma deve ser feito merge de uma environment branch que não seja a master. A dependência de outras branches deverá ser resolvida através de merges individuais. Exemplo: *feature/add-conditional-to-onboard* depende do redirect contido em *feature/add-redirect-to-nginx*, o desenvolvedor então irá executar os seguintes comandos:    
```bash
$ git checkout feature/add-conditional-to-onboard
$ git pull origin feature/add-conditional-to-onboard
$ git pull origin feature/add-redirect-to-nginx
```

- **Branch updating** - a atualização será feita através de merges periódicos na source branch ( master ) e na release branch ATUAL. 	JAMAIS deverá atualizar a branch com a outra branch a não ser essas.
## Hotfix branch
- **Preffix** - toda branch de hotfix deve ter o seguinte prefixo: hotfix/, exemplo: *hotfix/fix-product-inconsistency-on-event*
 
 - **Suffix** - o sufixo da branch deve conter ( em inglês ) a mudança que irá refletir no ambiente mergeado, exemplo *feature/add-redirect-to-nginx*.
 - **Source branch** - a branch deverá ser criada, ESTRITAMENTE, a partir da branch **master**.
 - **Integration** - O desenvolvedor ao se dar por satisfeito com a funcionalidade desenvolvida irá abrir um **PR** para **dev** e poderá mergea-la ( somente nesse passo ) e testa-la. O ciclo anterior irá se repetir até que a branch esteja digna de ser publicada em produção, nesse momento um **pull request** para a **master** deve ser realizado.
 - **Maintainer** - desenvolvedor
 - **Branch dependency** - Em hipótese alguma deve ser feito merge de qualquer outra branch. Essa branch não pode conter dependência de outra branch pois ela será mergeada diretamente na **master**.

- **Branch updating** - Essa branch só poderá ser atualizada através de merge da **master**.